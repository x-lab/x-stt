if [ ! -d "sphinxbase" ]; then
    git clone https://github.com/cmusphinx/sphinxbase.git
    cd sphinxbase && ./autogen.sh && cd ..
    cd sphinxbase && make CFLAGS="-O2 -fPIC" && cd ..
fi

if [ ! -d "pocketsphinx" ]; then
    git clone https://github.com/cmusphinx/pocketsphinx.git
    cd pocketsphinx && ./autogen.sh && cd ..
    cd pocketsphinx && make CFLAGS="-O2 -fPIC -Wno-error -Wno-implicit-function-declaration" && cd ..
fi
