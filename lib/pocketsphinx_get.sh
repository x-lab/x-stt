if [ ! -d "res/en" ]; then
    mkdir -p res/en
    cd res/en && wget http://svn.code.sf.net/p/cmusphinx/code/trunk/cmudict/sphinxdict/cmudict_SPHINX_40 && mv cmudict_SPHINX_40 lexicon.dict && cd ../..
    cd res/en && wget https://sourceforge.net/projects/cmusphinx/files/Acoustic%20and%20Language%20Models/US%20English/cmusphinx-en-us-5.2.tar.gz/download && tar -xf download && mv cmusphinx-en-us-5.2 acmodel  && cd ../..
fi
